package tcp

import "sync"

// AsyncSocket wraps any type that implements Socket.  It transforms the Socket
// interface into a push-not-pull interface that allows for message and close
// callbacks.  It is safe for concurrent use - even within callbacks.
type AsyncSocket struct {
	s Socket

	mx             *sync.RWMutex
	isClosed       bool
	messageHandler AsyncSocketMessageHandler
	closeHandler   AsyncSocketCloseHandler
	messages       [][]byte
	pendingClose   bool
}

// NewAsyncSocket creates a new AsyncSocket from the provided Socket.
func NewAsyncSocket(s Socket) *AsyncSocket {
	r := &AsyncSocket{
		s: s,

		mx: &sync.RWMutex{},
	}

	go r.receiveLoop()

	return r
}

// SetMessageHandler sets the message handler for the AsyncSocket.  If there are
// pending messages that have not been handled, these will be processed
// immediately.
func (s *AsyncSocket) SetMessageHandler(handler AsyncSocketMessageHandler) {
	messages := func() [][]byte {
		s.mx.Lock()
		defer s.mx.Unlock()

		s.messageHandler = handler
		messages := s.messages
		s.messages = nil
		return messages
	}()

	for _, message := range messages {
		handler(s, message)
	}
}

// SetCloseHandler sets the close handler for the AsyncSocket.  If the socket
// has been closed, the handler will be called immediately.
func (s *AsyncSocket) SetCloseHandler(handler AsyncSocketCloseHandler) {
	pendingClose := func() bool {
		s.mx.Lock()
		defer s.mx.Unlock()

		s.closeHandler = handler
		pendingClose := s.pendingClose
		s.pendingClose = false
		return pendingClose
	}()

	if pendingClose {
		handler(s)
	}
}

// Send simply sends the message on the underlying socket.
func (s *AsyncSocket) Send(message []byte) error {
	return s.s.Send(message)
}

// IsClosed returns true if the underlying socket is closed.
func (s *AsyncSocket) IsClosed() bool {
	s.mx.RLock()
	defer s.mx.RUnlock()

	return s.isClosed
}

// Close closes the underlying socket.
func (s *AsyncSocket) Close() error {
	if s.IsClosed() {
		return ErrSocketClosed
	}

	err := func() error {
		s.mx.Lock()
		defer s.mx.Unlock()

		if s.isClosed {
			return ErrSocketClosed
		}

		err := s.s.Close()
		s.isClosed = true

		if err == ErrSocketClosed {
			return nil
		}

		return err
	}()

	if err == ErrSocketClosed {
		return ErrSocketClosed
	}

	s.raiseCloseHandler()
	return err
}

func (s *AsyncSocket) receiveLoop() {
	for {
		message, err := s.s.Receive()
		if err != nil {
			s.Close()
			return
		}

		s.raiseMessageHandler(message)
	}
}

func (s *AsyncSocket) raiseMessageHandler(message []byte) {
	handler := func() AsyncSocketMessageHandler {
		s.mx.RLock()
		defer s.mx.RUnlock()

		return s.messageHandler
	}()

	if handler != nil {
		handler(s, message)
		return
	}

	handler = func() AsyncSocketMessageHandler {
		s.mx.Lock()
		defer s.mx.Unlock()

		if s.messageHandler != nil {
			return handler
		}

		s.messages = append(s.messages, message)
		return nil
	}()

	if handler != nil {
		handler(s, message)
	}
}

func (s *AsyncSocket) raiseCloseHandler() {
	handler := func() AsyncSocketCloseHandler {
		s.mx.RLock()
		defer s.mx.RUnlock()

		return s.closeHandler
	}()

	if handler != nil {
		handler(s)
		return
	}

	handler = func() AsyncSocketCloseHandler {
		s.mx.Lock()
		defer s.mx.Unlock()

		if s.closeHandler != nil {
			return handler
		}

		s.pendingClose = true
		return nil
	}()

	if handler != nil {
		handler(s)
	}
}
