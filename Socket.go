package tcp

import "time"

// Socket defines the methods that any Socket must implement.
type Socket interface {
	Send(message []byte) error
	Receive() ([]byte, error)

	SetReadDeadline(t time.Time) error
	SetWriteDeadline(t time.Time) error

	IsClosed() bool
	Close() error
}
