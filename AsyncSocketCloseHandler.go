package tcp

// AsyncSocketCloseHandler defines the shape of functions that handle
// AsyncSockets closing.
type AsyncSocketCloseHandler func(s *AsyncSocket)
