module gitlab.com/ljpx13/tcp

go 1.15

require (
	gitlab.com/ljpx13/ecdh25519 v0.1.1
	gitlab.com/ljpx13/test v0.1.6
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
)
