package tcp

import (
	"crypto/ed25519"
	"crypto/rand"
	"sync"
	"testing"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/has"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestSecureSocketSendAndReceive(t *testing.T) {
	performAsyncSecureSocketTest(
		t,
		func(s *SecureSocket) {
			err := s.Send([]byte("Hello, s2!"))
			test.That(t, err, is.Nil())

			message, err := s.Receive()
			test.That(t, err, is.Nil())
			test.That(t, string(message), is.EqualTo("Hello, s1!"))

			err = s.Close()
			test.That(t, err, is.Nil())
			test.That(t, s.IsClosed(), is.True())
		},
		func(s *SecureSocket) {
			message, err := s.Receive()
			test.That(t, err, is.Nil())

			err = s.Send([]byte("Hello, s1!"))
			test.That(t, err, is.Nil())

			message, err = s.Receive()
			test.That(t, err, is.NotNil())
			test.That(t, message, has.Length(0))
			test.That(t, s.IsClosed(), is.True())
		},
	)
}

func performAsyncSecureSocketTest(t *testing.T, f1, f2 func(s *SecureSocket)) {
	publicKey, privateKey, err := ed25519.GenerateKey(rand.Reader)
	test.That(t, err, is.Nil())

	s1, s2 := getSecureSocketPair()
	defer s1.Close()
	defer s2.Close()

	wg := &sync.WaitGroup{}
	wg.Add(2)

	go func() {
		defer wg.Done()

		err := s1.HandshakeWithPrivate(&privateKey)
		test.That(t, err, is.Nil())

		f1(s1)
	}()

	go func() {
		defer wg.Done()

		err := s2.HandshakeWithPublic(&publicKey)
		test.That(t, err, is.Nil())

		f2(s2)
	}()

	wg.Wait()
}

func getSecureSocketPair() (*SecureSocket, *SecureSocket) {
	s1, s2 := getLengthPrefixedSocketPair()
	return NewSecureSocket(s1), NewSecureSocket(s2)
}
