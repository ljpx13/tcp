package tcp

import (
	"testing"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestPacketBool(t *testing.T) {
	p1 := NewPacket()
	p1.WriteBool(true)
	buf := p1.Bytes()
	p2 := PacketFromBytes(buf)
	test.That(t, p2.ReadBool(), is.True())
}

func TestPacketString(t *testing.T) {
	p1 := NewPacket()
	p1.WriteString("Hello, World!")
	buf := p1.Bytes()
	p2 := PacketFromBytes(buf)
	test.That(t, p2.ReadString(), is.EqualTo("Hello, World!"))
}

func TestPacketInt8(t *testing.T) {
	p1 := NewPacket()
	p1.WriteInt8(42)
	buf := p1.Bytes()
	p2 := PacketFromBytes(buf)
	test.That(t, p2.ReadInt8(), is.EqualTo(int8(42)))
}

func TestPacketUInt8(t *testing.T) {
	p1 := NewPacket()
	p1.WriteUInt8(42)
	buf := p1.Bytes()
	p2 := PacketFromBytes(buf)
	test.That(t, p2.ReadUInt8(), is.EqualTo(uint8(42)))
}

func TestPacketInt16(t *testing.T) {
	p1 := NewPacket()
	p1.WriteInt16(42)
	buf := p1.Bytes()
	p2 := PacketFromBytes(buf)
	test.That(t, p2.ReadInt16(), is.EqualTo(int16(42)))
}

func TestPacketUInt16(t *testing.T) {
	p1 := NewPacket()
	p1.WriteUInt16(42)
	buf := p1.Bytes()
	p2 := PacketFromBytes(buf)
	test.That(t, p2.ReadUInt16(), is.EqualTo(uint16(42)))
}

func TestPacketInt32(t *testing.T) {
	p1 := NewPacket()
	p1.WriteInt32(42)
	buf := p1.Bytes()
	p2 := PacketFromBytes(buf)
	test.That(t, p2.ReadInt32(), is.EqualTo(int32(42)))
}

func TestPacketUInt32(t *testing.T) {
	p1 := NewPacket()
	p1.WriteUInt32(42)
	buf := p1.Bytes()
	p2 := PacketFromBytes(buf)
	test.That(t, p2.ReadUInt32(), is.EqualTo(uint32(42)))
}

func TestPacketInt64(t *testing.T) {
	p1 := NewPacket()
	p1.WriteInt64(42)
	buf := p1.Bytes()
	p2 := PacketFromBytes(buf)
	test.That(t, p2.ReadInt64(), is.EqualTo(int64(42)))
}

func TestPacketUInt64(t *testing.T) {
	p1 := NewPacket()
	p1.WriteUInt64(42)
	buf := p1.Bytes()
	p2 := PacketFromBytes(buf)
	test.That(t, p2.ReadUInt64(), is.EqualTo(uint64(42)))
}

func TestPacketFloat32(t *testing.T) {
	p1 := NewPacket()
	p1.WriteFloat32(42.5)
	buf := p1.Bytes()
	p2 := PacketFromBytes(buf)
	test.That(t, p2.ReadFloat32(), is.EqualTo(float32(42.5)))
}

func TestPacketFloat64(t *testing.T) {
	p1 := NewPacket()
	p1.WriteFloat64(42.5)
	buf := p1.Bytes()
	p2 := PacketFromBytes(buf)
	test.That(t, p2.ReadFloat64(), is.EqualTo(42.5))
}
