package tcp

// Packetable defines the methods that any type capable of being serialized to
// and from a packet must implement.
type Packetable interface {
	ToPacket() *Packet
	FromPacket(p *Packet)
}
