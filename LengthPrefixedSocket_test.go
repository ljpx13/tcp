package tcp

import (
	"net"
	"sync"
	"testing"
	"time"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/has"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestLengthPrefixedSocketReturnsErrSocketClosedAfterCallingClose(t *testing.T) {
	// Arrange.
	s1, s2 := getLengthPrefixedSocketPair()
	defer s2.Close()

	// Act.
	err := s1.Close()
	test.That(t, err, is.Nil())
	test.That(t, s1.IsClosed(), is.True())

	// Assert.
	err1 := s1.Close()
	_, err2 := s1.Receive()
	err3 := s1.Send([]byte{})
	err4 := s1.SetReadDeadline(time.Time{})
	err5 := s1.SetWriteDeadline(time.Time{})

	test.That(t, err1, is.EqualTo(ErrSocketClosed))
	test.That(t, err2, is.EqualTo(ErrSocketClosed))
	test.That(t, err3, is.EqualTo(ErrSocketClosed))
	test.That(t, err4, is.EqualTo(ErrSocketClosed))
	test.That(t, err5, is.EqualTo(ErrSocketClosed))
}

func TestLengthPrefixedSocketSendAndReceive(t *testing.T) {
	performAsyncLengthPrefixedSocketTest(
		func(s *LengthPrefixedSocket) {
			err := s.Send([]byte("Hello, s2!"))
			test.That(t, err, is.Nil())

			message, err := s.Receive()
			test.That(t, err, is.Nil())
			test.That(t, string(message), is.EqualTo("Hello, s1!"))

			err = s.Close()
			test.That(t, err, is.Nil())
			test.That(t, s.IsClosed(), is.True())
		},
		func(s *LengthPrefixedSocket) {
			message, err := s.Receive()
			test.That(t, err, is.Nil())

			err = s.Send([]byte("Hello, s1!"))
			test.That(t, err, is.Nil())

			message, err = s.Receive()
			test.That(t, err, is.NotNil())
			test.That(t, message, has.Length(0))
			test.That(t, s.IsClosed(), is.True())
		},
	)
}

func performAsyncLengthPrefixedSocketTest(f1, f2 func(s *LengthPrefixedSocket)) {
	s1, s2 := getLengthPrefixedSocketPair()
	defer s1.Close()
	defer s2.Close()

	wg := &sync.WaitGroup{}
	wg.Add(2)

	go func() {
		defer wg.Done()
		f1(s1)
	}()

	go func() {
		defer wg.Done()
		f2(s2)
	}()

	wg.Wait()
}

func getLengthPrefixedSocketPair() (*LengthPrefixedSocket, *LengthPrefixedSocket) {
	conn1, conn2 := net.Pipe()
	return NewLengthPrefixedSocket(conn1), NewLengthPrefixedSocket(conn2)
}
