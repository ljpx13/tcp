package tcp

// AsyncSocketMessageHandler defines the shape of functions that handle messages
// from AsyncSockets.
type AsyncSocketMessageHandler func(s *AsyncSocket, message []byte)
