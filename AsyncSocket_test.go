package tcp

import (
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestSocketCloseHandlerCalledOnlyOnce(t *testing.T) {
	// Arrange.
	s1, s2 := getAsyncSocketPair()
	wg1 := &sync.WaitGroup{}
	wg1.Add(1)

	wg2 := &sync.WaitGroup{}
	wg2.Add(4)

	c := int32(0)
	s1.SetCloseHandler(func(_ *AsyncSocket) {
		atomic.AddInt32(&c, 1)
		wg2.Done()
	})
	s2.SetCloseHandler(func(_ *AsyncSocket) {
		atomic.AddInt32(&c, 1)
		wg2.Done()
	})

	go func() {
		wg1.Wait()
		s1.Close()
		wg2.Done()
	}()

	go func() {
		wg1.Wait()
		s1.Close()
		wg2.Done()
	}()

	// Act.
	wg1.Done()
	wg2.Wait()

	// Assert.
	test.That(t, c, is.EqualTo(int32(2)))
	test.That(t, s2.IsClosed(), is.True())
}

func TestSocketSendAndReceive(t *testing.T) {
	// Arrange.
	c := int32(0)

	// Act.
	runSideBySideSocketTest(
		func(s *AsyncSocket, wg *sync.WaitGroup) {
			wg.Add(1)

			s.SetMessageHandler(func(_ *AsyncSocket, message []byte) {
				test.That(t, string(message), is.EqualTo("Hello, s1!"))

				err := s.Send([]byte("Hello, s2!"))
				test.That(t, err, is.Nil())

				atomic.AddInt32(&c, 1)
			})

			s.SetCloseHandler(func(_ *AsyncSocket) {
				atomic.AddInt32(&c, 1)
				wg.Done()
			})
		},
		func(s *AsyncSocket, _ *sync.WaitGroup) {
			s.SetMessageHandler(func(_ *AsyncSocket, message []byte) {
				test.That(t, string(message), is.EqualTo("Hello, s2!"))
				atomic.AddInt32(&c, 1)

				err := s.Close()
				test.That(t, err, is.Nil())
			})

			s.SetCloseHandler(func(_ *AsyncSocket) {
				atomic.AddInt32(&c, 1)
			})

			err := s.Send([]byte("Hello, s1!"))
			test.That(t, err, is.Nil())
		},
	)

	// Assert.
	test.That(t, c, is.EqualTo(int32(4)))
}

func TestSocketMessageQueue(t *testing.T) {
	// Arrange.
	c := int32(0)

	// Act.
	runSideBySideSocketTest(
		func(s *AsyncSocket, _ *sync.WaitGroup) {
			time.Sleep(time.Millisecond * 50)

			s.SetMessageHandler(func(s *AsyncSocket, message []byte) {
				atomic.AddInt32(&c, 1)

				if message[0] == 1 {
					err := s.Send([]byte{1})
					test.That(t, err, is.Nil())
				} else {
					err := s.Close()
					test.That(t, err, is.Nil())
				}
			})

			s.SetCloseHandler(func(_ *AsyncSocket) {
				atomic.AddInt32(&c, 1)
			})
		},
		func(s *AsyncSocket, wg *sync.WaitGroup) {
			wg.Add(1)

			s.SetMessageHandler(func(_ *AsyncSocket, message []byte) {
				atomic.AddInt32(&c, 1)
				err := s.Send([]byte{2})
				test.That(t, err, is.Nil())
			})

			s.SetCloseHandler(func(_ *AsyncSocket) {
				atomic.AddInt32(&c, 1)
				wg.Done()
			})

			err := s.Send([]byte{1})
			test.That(t, err, is.Nil())
		},
	)

	// Assert.
	test.That(t, c, is.EqualTo(int32(5)))
}

func runSideBySideSocketTest(s1Func, s2Func func(s *AsyncSocket, wg *sync.WaitGroup)) {
	s1, s2 := getAsyncSocketPair()
	wg := &sync.WaitGroup{}
	wg.Add(2)

	go func() {
		s1Func(s1, wg)
		wg.Done()
	}()

	go func() {
		s2Func(s2, wg)
		wg.Done()
	}()

	wg.Wait()
}

func getAsyncSocketPair() (*AsyncSocket, *AsyncSocket) {
	s1, s2 := getLengthPrefixedSocketPair()
	return NewAsyncSocket(s1), NewAsyncSocket(s2)
}
