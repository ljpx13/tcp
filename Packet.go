package tcp

import (
	"bytes"
	"encoding/binary"
	"math"
)

// Packet is a structure that allows writing and reading of various data types
// to a byte stream.  All Read* methods on Packet will return their default
// value if the packet runs out of bytes to read.
type Packet struct {
	buf *bytes.Buffer
}

// NewPacket creates a new, empty packet.
func NewPacket() *Packet {
	return PacketFromBytes([]byte{})
}

// PacketFromBytes creates a Packet from the provided raw bytes.
func PacketFromBytes(buf []byte) *Packet {
	return &Packet{
		buf: bytes.NewBuffer(buf),
	}
}

// Bytes returns the raw byte slice representation of the packet.
func (p *Packet) Bytes() []byte {
	return p.buf.Bytes()
}

// WriteBool writes a bool to the packet.
func (p *Packet) WriteBool(x bool) {
	b := byte(0)
	if x {
		b = byte(1)
	}

	p.buf.WriteByte(b)
}

// ReadBool reads a bool from the packet.
func (p *Packet) ReadBool() bool {
	b, err := p.buf.ReadByte()
	if err != nil {
		return false
	}

	if b == byte(0) {
		return false
	}

	return true
}

// WriteString writes a string to the packet.  If the length of the string is
// too large, WriteString will do nothing.
func (p *Packet) WriteString(x string) {
	if len(x) > (1<<16)-1 {
		return
	}

	len16 := uint16(len(x))
	p.WriteUInt16(len16)
	p.buf.Write([]byte(x))
}

// ReadString reads a string from the packet.
func (p *Packet) ReadString() string {
	len16 := p.ReadUInt16()

	buf := make([]byte, len16)
	if _, err := p.buf.Read(buf); err != nil {
		return ""
	}

	return string(buf)
}

// WriteInt8 writes an int8 to the packet.
func (p *Packet) WriteInt8(x int8) {
	p.WriteUInt8(uint8(x))
}

// ReadInt8 reads an int8 from the packet.
func (p *Packet) ReadInt8() int8 {
	return int8(p.ReadUInt8())
}

// WriteUInt8 writes a uint8 to the packet.
func (p *Packet) WriteUInt8(x uint8) {
	p.buf.WriteByte(x)
}

// ReadUInt8 reads a uint8 from the packet.
func (p *Packet) ReadUInt8() uint8 {
	x, _ := p.buf.ReadByte()
	return x
}

// WriteInt16 writes an int16 to the packet.
func (p *Packet) WriteInt16(x int16) {
	p.WriteUInt16(uint16(x))
}

// ReadInt16 reads an int16 from the packet.
func (p *Packet) ReadInt16() int16 {
	return int16(p.ReadUInt16())
}

// WriteUInt16 writes a uint16 to the packet.
func (p *Packet) WriteUInt16(x uint16) {
	buf := make([]byte, 2)
	binary.LittleEndian.PutUint16(buf, x)
	p.buf.Write(buf)
}

// ReadUInt16 reads a uint16 from the packet.
func (p *Packet) ReadUInt16() uint16 {
	buf := make([]byte, 2)
	_, err := p.buf.Read(buf)
	if err != nil {
		return 0
	}

	return binary.LittleEndian.Uint16(buf)
}

// WriteInt32 writes an int32 to the packet.
func (p *Packet) WriteInt32(x int32) {
	p.WriteUInt32(uint32(x))
}

// ReadInt32 reads an int32 from the packet.
func (p *Packet) ReadInt32() int32 {
	return int32(p.ReadUInt32())
}

// WriteUInt32 writes a uint32 to the packet.
func (p *Packet) WriteUInt32(x uint32) {
	buf := make([]byte, 4)
	binary.LittleEndian.PutUint32(buf, x)
	p.buf.Write(buf)
}

// ReadUInt32 reads a uint32 from the packet.
func (p *Packet) ReadUInt32() uint32 {
	buf := make([]byte, 4)
	_, err := p.buf.Read(buf)
	if err != nil {
		return 0
	}

	return binary.LittleEndian.Uint32(buf)
}

// WriteInt64 writes an int64 to the packet.
func (p *Packet) WriteInt64(x int64) {
	p.WriteUInt64(uint64(x))
}

// ReadInt64 reads an int64 from the packet.
func (p *Packet) ReadInt64() int64 {
	return int64(p.ReadUInt64())
}

// WriteUInt64 writes a uint64 to the packet.
func (p *Packet) WriteUInt64(x uint64) {
	buf := make([]byte, 8)
	binary.LittleEndian.PutUint64(buf, x)
	p.buf.Write(buf)
}

// ReadUInt64 reads a uint64 from the packet.
func (p *Packet) ReadUInt64() uint64 {
	buf := make([]byte, 8)
	_, err := p.buf.Read(buf)
	if err != nil {
		return 0
	}

	return binary.LittleEndian.Uint64(buf)
}

// WriteFloat32 writes a float32 to the packet.
func (p *Packet) WriteFloat32(x float32) {
	y := math.Float32bits(x)
	p.WriteUInt32(y)
}

// ReadFloat32 reads a float32 from the packet.
func (p *Packet) ReadFloat32() float32 {
	y := p.ReadUInt32()
	return math.Float32frombits(y)
}

// WriteFloat64 writes a float64 to the packet.
func (p *Packet) WriteFloat64(x float64) {
	y := math.Float64bits(x)
	p.WriteUInt64(y)
}

// ReadFloat64 reads a float64 from the packet.
func (p *Packet) ReadFloat64() float64 {
	y := p.ReadUInt64()
	return math.Float64frombits(y)
}
