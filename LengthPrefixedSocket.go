package tcp

import (
	"errors"
	"fmt"
	"io"
	"net"
	"sync"
	"time"
)

// LengthPrefixedSocket is an implementation of Socket that produces discrete
// messages by prefixing the message length prior to sending.  It uses 16 bits
// to represent the message length, thus a hard-limit of ~64kB per message is
// enforced.  It is safe for concurrent use, except for Receive(), which should
// only ever be called on a single goroutine.
type LengthPrefixedSocket struct {
	conn net.Conn

	mx       *sync.RWMutex
	isClosed bool
}

var _ Socket = &LengthPrefixedSocket{}

// MaximumMessageSize is the maximum size allowed for messages.
const MaximumMessageSize = (1 << 16) - 1

// ErrMessageTooLong is returned when a message to be sent is too large.
var ErrMessageTooLong = fmt.Errorf("the provided message is too large")

// ErrSocketClosed is returned when an operation is performed on a closed
// socket.
var ErrSocketClosed = errors.New("cannot perform operation on a closed socket")

// NewLengthPrefixedSocket creates a new LengthPrefixedSocket wrapping the
// provided net.Conn.
func NewLengthPrefixedSocket(conn net.Conn) *LengthPrefixedSocket {
	return &LengthPrefixedSocket{
		conn: conn,

		mx: &sync.RWMutex{},
	}
}

// SetReadDeadline sets the read deadline on the underlying net.Conn.
func (s *LengthPrefixedSocket) SetReadDeadline(t time.Time) error {
	if s.IsClosed() {
		return ErrSocketClosed
	}

	return s.conn.SetReadDeadline(t)
}

// SetWriteDeadline sets the write deadline on the underlying net.Conn.
func (s *LengthPrefixedSocket) SetWriteDeadline(t time.Time) error {
	if s.IsClosed() {
		return ErrSocketClosed
	}

	return s.conn.SetWriteDeadline(t)
}

// Send sends a message using the underlying net.Conn.  If Send returns an
// error, and that error is neither ErrSocketClosed or ErrMessageTooLong, the
// socket will be closed.
func (s *LengthPrefixedSocket) Send(message []byte) error {
	if s.IsClosed() {
		return ErrSocketClosed
	}

	if len(message) > MaximumMessageSize {
		return ErrMessageTooLong
	}

	messageLength16 := uint16(len(message))
	messageLengthBuf := []byte{
		byte(messageLength16 >> 8),
		byte(messageLength16 << 8 >> 8),
	}

	message = append(messageLengthBuf, message...)
	_, err := s.conn.Write(message)
	if err != nil {
		s.Close()
		return err
	}

	return nil
}

// Receive receives a message using the underlying net.Conn.  If Receive returns
// an error, and that error is not ErrSocketClosed, the socked will be closed.
// Receive is not safe for use concurrently.
func (s *LengthPrefixedSocket) Receive() ([]byte, error) {
	if s.IsClosed() {
		return nil, ErrSocketClosed
	}

	messageLengthBuf := make([]byte, 2)
	_, err := io.ReadAtLeast(s.conn, messageLengthBuf, len(messageLengthBuf))
	if err != nil {
		s.Close()
		return nil, err
	}

	messageLength := uint16(messageLengthBuf[0]) << 8
	messageLength += uint16(messageLengthBuf[1])

	messageBuf := make([]byte, messageLength)
	_, err = io.ReadAtLeast(s.conn, messageBuf, len(messageBuf))
	if err != nil {
		s.Close()
		return nil, err
	}

	return messageBuf, nil
}

// IsClosed returns true if the socket is closed and thus cannot be used.
func (s *LengthPrefixedSocket) IsClosed() bool {
	s.mx.RLock()
	defer s.mx.RUnlock()

	return s.isClosed
}

// Close closes the socket.
func (s *LengthPrefixedSocket) Close() error {
	if s.IsClosed() {
		return ErrSocketClosed
	}

	s.mx.Lock()
	defer s.mx.Unlock()

	if s.isClosed {
		return ErrSocketClosed
	}

	err := s.conn.Close()
	s.isClosed = true
	return err
}
