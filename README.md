![](./icon.png)

# tcp

Package `tcp` implements a number of different wrappers around `net.Conn` that
enable easier use of TCP.
