package tcp

import (
	"crypto/cipher"
	"crypto/ed25519"
	"crypto/rand"
	"crypto/sha256"
	"errors"
	"sync"
	"time"

	"gitlab.com/ljpx13/ecdh25519"
	"golang.org/x/crypto/chacha20poly1305"
)

// SecureSocket is an implementation of Socket that provides a secure tunnel
// using X25519/Ed25519/ChaCha20Poly1305.  It wraps another Socket.
type SecureSocket struct {
	s Socket

	mx   *sync.RWMutex
	aead cipher.AEAD
}

var _ Socket = &SecureSocket{}

// ErrHandshakeRequired is returned when Send or Receive is called prior to the
// handshake completing.
var ErrHandshakeRequired = errors.New("cannot send or receive prior to handshake")

// ErrMessageTooShort is returned when a message is received that is too short
// to be correctly encrypted message.
var ErrMessageTooShort = errors.New("the received message is too short to be valid")

// ErrInvalidSignature is returned when a signature during handshake was not
// valid.
var ErrInvalidSignature = errors.New("the signature is not valid")

// NewSecureSocket creates a new SecureSocket around the provided underlying
// Socket.
func NewSecureSocket(s Socket) *SecureSocket {
	return &SecureSocket{
		s: s,

		mx: &sync.RWMutex{},
	}
}

// SetReadDeadline sets the read deadline on the underlying Socket.
func (s *SecureSocket) SetReadDeadline(t time.Time) error {
	return s.s.SetReadDeadline(t)
}

// SetWriteDeadline sets the write deadline on the underlying Socket.
func (s *SecureSocket) SetWriteDeadline(t time.Time) error {
	return s.s.SetWriteDeadline(t)
}

// HandshakeWithPrivate handshakes as the owner of the private key.
func (s *SecureSocket) HandshakeWithPrivate(prv *ed25519.PrivateKey) error {
	ephemeralPrivateKey, err := ecdh25519.GenerateKey(rand.Reader)
	if err != nil {
		s.Close()
		return err
	}

	ephemeralPublicKeyRaw := ephemeralPrivateKey.PublicKey().ToBytes()
	signature := ed25519.Sign(*prv, ephemeralPublicKeyRaw)

	remotePublicKeyRaw, err := s.s.Receive()
	if err != nil {
		s.Close()
		return err
	}

	remotePublicKey, err := ecdh25519.PublicKeyFromBytes(remotePublicKeyRaw)
	if err != nil {
		s.Close()
		return err
	}

	sharedSecret := ephemeralPrivateKey.SharedSecretWith(remotePublicKey)
	aeadKey := sha256.Sum256(sharedSecret)
	aead, err := chacha20poly1305.New(aeadKey[:])
	if err != nil {
		s.Close()
		return err
	}

	if err := s.s.Send(append(ephemeralPublicKeyRaw, signature...)); err != nil {
		s.Close()
		return err
	}

	s.mx.Lock()
	defer s.mx.Unlock()

	s.aead = aead
	return nil
}

// HandshakeWithPublic handshakes against an owner of a private key using their
// public key.
func (s *SecureSocket) HandshakeWithPublic(pub *ed25519.PublicKey) error {
	ephemeralPrivateKey, err := ecdh25519.GenerateKey(rand.Reader)
	if err != nil {
		s.Close()
		return err
	}

	ephemeralPublicKeyRaw := ephemeralPrivateKey.PublicKey().ToBytes()
	if err := s.s.Send(ephemeralPublicKeyRaw); err != nil {
		s.Close()
		return err
	}

	publicKeyAndSignature, err := s.s.Receive()
	if len(publicKeyAndSignature) != ecdh25519.PublicKeySize+ed25519.SignatureSize {
		s.Close()
		return ErrMessageTooShort
	}

	publicKeyRaw := publicKeyAndSignature[:ecdh25519.PublicKeySize]
	signature := publicKeyAndSignature[ecdh25519.PublicKeySize:]
	if !ed25519.Verify(*pub, publicKeyRaw, signature) {
		s.Close()
		return ErrInvalidSignature
	}

	remotePublicKey, err := ecdh25519.PublicKeyFromBytes(publicKeyRaw)
	if err != nil {
		s.Close()
		return err
	}

	sharedSecret := ephemeralPrivateKey.SharedSecretWith(remotePublicKey)
	aeadKey := sha256.Sum256(sharedSecret)
	aead, err := chacha20poly1305.New(aeadKey[:])
	if err != nil {
		s.Close()
		return err
	}

	s.mx.Lock()
	defer s.mx.Unlock()

	s.aead = aead
	return nil
}

// Send encrypts a message using the session key and sends it on the underlying
// Socket.
func (s *SecureSocket) Send(message []byte) error {
	aead := func() cipher.AEAD {
		s.mx.RLock()
		defer s.mx.RUnlock()

		return s.aead
	}()

	if aead == nil {
		return ErrHandshakeRequired
	}

	nonce := make([]byte, aead.NonceSize())
	_, err := rand.Read(nonce)
	if err != nil {
		return err
	}

	ciphertext := aead.Seal(nil, nonce, message, nil)
	ciphertextAndNonce := append(nonce, ciphertext...)
	return s.s.Send(ciphertextAndNonce)
}

// Receive receives a message from the underlying socket and decrypts it using
// the session key.
func (s *SecureSocket) Receive() ([]byte, error) {
	aead := func() cipher.AEAD {
		s.mx.RLock()
		defer s.mx.RUnlock()

		return s.aead
	}()

	if aead == nil {
		return nil, ErrHandshakeRequired
	}

	message, err := s.s.Receive()
	if err != nil {
		return nil, err
	}

	if len(message) < aead.NonceSize() {
		s.Close()
		return nil, ErrMessageTooShort
	}

	nonce := message[:aead.NonceSize()]
	ciphertext := message[aead.NonceSize():]
	plaintext, err := aead.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		s.Close()
		return nil, err
	}

	return plaintext, nil
}

// IsClosed returns true if the socket is closed and thus cannot be used.
func (s *SecureSocket) IsClosed() bool {
	return s.s.IsClosed()
}

// Close closes the socket.
func (s *SecureSocket) Close() error {
	return s.s.Close()
}
